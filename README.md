<h3><b>This project was developed with Kotlin and uses the following Technologies</b></h3><br>
<hr>
<b>language:</b>Kotlin<br>
<b>architecture:</b>MVI, Clean Architecture, Single Activity, offline first<br>
<b>Jetpack:</b> Navigation, DataBinding, ConstraintLayout<br>
<b>Connect to Server:</b> Coroutine, Retrofit<br>
<b>Dependency Injection:</b> Dagge hilt<br>
<b>Image Loader:</b> Coil<br>
<hr>
![Screenshot_1](https://gitlab.com/javidDeveloper/themoviedb/-/raw/master/GIF-210924_200141.gif)
